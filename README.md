Lua interface to QT
===================

[Lua](http://www.lua.org) interface to [Qt](https://www.qt.io).

Supported Lua versions: ≥5.4.6

Supported Qt versions: 5.15

Design
------

This Qt-Lua interface has been designed with the idea to make Lua available to Qt 
(in contrast to making Qt available to Lua). 

This is a fork of the [Torch qtlua](https://github.com/torch/qtlua) repository.

To avoid clashes, with other existing Qt-interfaces, it has been renamed to 'giralua' 
(from "gira" or "girar", Portuguese for "cute" or "spinning", respectively).

Lua API documentation
---------------------

This package includes several useful sub-packages:

  - [qt](doc/qt.md): the global Qt system
  - [qtcore](doc/qtcore.md): Core interface
  - [qtgui](doc/qtgui.md): GUI
  - [qtsvg](doc/qtsvg.md): Qt SVG support
  - [qtwidget](doc/qtwidget.md): widgets and events
  - [qtuiloader](doc/qtuiloader.md): QT Designer UI files loader

See `qt` and `qtcore` for some binding design notes. A [readthedocs rendered](http://qtlua.readthedocs.io/en/latest/) version of these documents is also available.

To the best of our knowledge, Torch's QtLua implementation is unrelated to -- and very likely incompatible with -- any other Qt-Lua interop projects including:

  - http://www.nongnu.org/libqtlua/
  - https://github.com/tdzl2003/luaqt

C++ API documentation
---------------------

to be done
