# Version settings

# GiraLua
set(GiraLua_VERSION 0.2.0)

# Lua
set(GiraLua_LUA_MAJOR 5)
set(GiraLua_LUA_MINOR 4)

# Qt
set(GiraLua_QT_MAJOR 6)
set(GiraLua_QT_MINOR 0)

set(GiraLua_QT_TAG Qt${GiraLua_QT_MAJOR})       
set(GiraLua_QT_COMPONENTS Core Gui Network Sql Svg Widgets PrintSupport UiTools)