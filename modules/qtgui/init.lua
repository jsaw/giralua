require 'qt5'
require 'qt5.core'

if qt5 and qt5.qApp and qt5.qApp:runsWithoutGraphics() then
   print("qlua: not loading module qtgui (running with -nographics)")
   return
end

qt5.require 'gui'


