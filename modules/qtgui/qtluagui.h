#ifndef QTLUAGUI_H
#define QTLUAGUI_H

#include <QtWidgets/QAction>
#include <QtCore/QCoreApplication>
#include <QtCore/QObject>
#include <QtCore/QPointer>
#include "qtgui.h"

class QTGUI_API QtLuaAction: public QAction {
    Q_OBJECT
    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled)
    Q_PROPERTY(bool autoDisable READ autoDisable WRITE setAutoDisable)

public:
    QtLuaAction(QtLuaEngine *e = 0, QObject *p = 0);
    bool isEnabled() const {
        return enabled;
    }
    bool autoDisable() const {
        return !override;
    }
public slots:
    void stateChanged();
    void setEngine(QtLuaEngine *e);
    void setEnabled(bool b) {
        enabled = b;
        stateChanged();
    }
    void setDisabled(bool b) {
        enabled = !b;
        stateChanged();
    }
    void setAutoDisable(bool b) {
        override = !b;
        stateChanged();
    }
private:
    bool enabled;
    bool override;
    QPointer<QtLuaEngine> engine;
};

#endif
