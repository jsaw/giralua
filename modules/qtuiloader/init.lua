
require 'qt5.core'

if qt5 and qt5.qApp and qt5.qApp:runsWithoutGraphics() then
   print("giralua: not loading module uiloader (running with -nographics)")
   return
end

qt5.require 'uiloader'

local qt5 = qt5

qtuiloader = qtuiloader or {} -- FIXME what's this???
local M = qtuiloader

local theloader = nil

function M.loader()
   if (not theloader or not theloader:tobool()) then
      theloader = qt5.QUiLoader()
   end
   return theloader;
end

local loaderFunctions = {
   "load", "availableWidgets", "createWidget",
   "createLayout", "createAction", "createActionGroup" }

for i = 1,#loaderFunctions do
   local f = loaderFunctions[i]
   M[f] = function(...)
              local uiloader = M.loader()
              return uiloader[f](uiloader,...)
           end
end

return M
