#ifndef QTSVG_H
#define QTSVG_H

#include "lua.hpp"

#include "qtluaengine.h"
#include "qtluautils.h"

#ifdef _WIN32
# ifdef libqtsvg_EXPORTS
#  define QTSVG_API __declspec(dllexport)
# else
#  define QTSVG_API __declspec(dllimport)
# endif
#else
# define QTSVG_API /**/
#endif

#define LUA_EXTERNC extern "C"

LUA_EXTERNC QTSVG_API int luaopen_libqtsvg(lua_State *L);

#endif
