#ifndef QTWIDGET_H
#define QTWIDGET_H

#include "lua.hpp"

#include "qtluaengine.h"
#include "qtluautils.h"

#ifdef _WIN32
#   ifdef libqtwidget_EXPORTS
#       define QTWIDGET_API __declspec(dllexport)
#   else
#       define QTWIDGET_API __declspec(dllimport)
#   endif
#else
#   define QTWIDGET_API /**/
#endif

#define LUA_EXTERNC extern "C"

LUA_EXTERNC QTWIDGET_API int luaopen_libqtwidget(lua_State *L);

#endif
