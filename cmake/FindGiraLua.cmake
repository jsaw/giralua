if(GiraLua_FIND_VERSION_MAJOR AND NOT GiraLua_FIND_VERSION_MINOR)
    message("Version major and minor are required.")
endif()

if(GiraLua_FIND_VERSION_MAJOR AND GiraLua_FIND_VERSION_PATCH)
    message(AUTHOR_WARNING "Patch version ignored.")
endif()

include(GiraLuaConfig-${GiraLua_FIND_VERSION_MAJOR}.${GiraLua_FIND_VERSION_MINOR}
    OPTIONAL RESULT_VARIABLE GiraLuaConfig_FOUND)
