# MACRO_ADD_QTLUA_MODULE(<modulename> <...sourcefiles..>)
# Adds a target for a qtlua module.
# Links with the right libraries (lua,qtlua,qt4). 
# Adds the right include dirs and definitions.
# Declares the install rule for the target.
macro(macro_add_qtlua_module modulename)
	cmake_parse_arguments(_ "" "" "LINK" ${ARGN})

    add_library(${modulename} MODULE ${__UNPARSED_ARGUMENTS})
    target_link_libraries(${modulename}
        GiraLua::Engine
        ${__LINK})

	set_target_properties(${modulename}
		PROPERTIES
			PREFIX ""
			OUTPUT_NAME ${giralua_qt_tag}/libqt${modulename})

    install(TARGETS "${modulename}" 
        RUNTIME DESTINATION ${GiraLua_CPATH_DIR}/${giralua_qt_tag} COMPONENT "runtime"
        LIBRARY DESTINATION ${GiraLua_CPATH_DIR}/${giralua_qt_tag} COMPONENT "runtime")
endmacro(macro_add_qtlua_module)

# MACRO_INSTALL_QTLUA_FILES(<modulename> <...luafiles..>)
# Install lua files for a module.
macro(macro_install_qtlua_files modulename)
    foreach(file ${ARGN})
        configure_file(${file} ${EXECUTABLE_OUTPUT_PATH}/${giralua_qt_tag}/${modulename}/${file} COPYONLY)
    endforeach(file)

    install(FILES ${ARGN} 
        DESTINATION "${GiraLua_PATH_DIR}/${giralua_qt_tag}/${modulename}" COMPONENT "runtime")
endmacro(macro_install_qtlua_files)


